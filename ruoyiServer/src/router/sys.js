/*
 * @Description: ------ 文件描述 ------
 * @Creater: snows_l snows_l@163.com
 * @Date: 2023-04-16 18:41:28
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2024-03-20 09:56:34
 * @FilePath: /ruoyi+node/ruoyiServer/src/router/sys.js
 */
const express = require('express');

// 引入mysql
const mysql = require('mysql');
//
// 设置token
const { generateToken, verifyToken } = require('../../utils/handleToken');

// 解密前端的加密密码
const { decryptPwd } = require('../../utils/node-rsa');

// 数据库操作
const db = require('../../utils/connDB');

// list转tree的函数
const { tranListToTree, perfMenuObj } = require('../../utils/common');

const router = express.Router();

/**
 * @description: 登录接口
 * @return {*}
 */
router.post('/login', (req, res) => {
  // 前端读取到的用户密码
  const user = req.body;
  const sql = `select * from sys_user where user_name = '${user.username}'`;
  db.queryAsync(sql)
    .then(response => {
      let dbUser = response.results[0];
      if (user.username == dbUser.user_name && user.password == decryptPwd(dbUser.password)) {
        const token = generateToken(user);
        res.send({
          code: 200,
          msg: '登录成功',
          token
        });
        return;
      } else {
        res.send({
          code: 500,
          msg: '密码错误'
        });
      }
    })
    .catch(err => {
      res.send({
        code: 500,
        msg: '系统异常， 请联系管理！'
      });
    });
});

// 获取用户信息
router.get('/getInfo', (req, res) => {
  if (req.headers.authorization) {
    let token = req.headers.authorization.split(' ')[1];
    const user = verifyToken(token);
    if (user) {
      const sql = `select* from sys_user where user_name = '${user.username}'`;
      db.queryAsync(sql).then(resss => {
        res.send({ code: 200, msg: '操作成功', user: resss.results[0] });
      });
    } else {
      res.status(401).send({ code: '401', msg: '登录失效，请重新登录' });
    }
  }
});

/**
 * @description: 获取用户菜单
 *
 * @step1 { Object } loginInfo ==> 根据token拿到登录的信息
 * @step2 { String } user ==> 根据登录的信息 username 查询用户的用户对象 并取到user_id
 * @step3 { String } role_id ==> 根据user_id 查询用户 role_id
 * @step4 { Array } menuIds ==> 根据role_id 查询当前角色绑定的 memus 并取到menuIds
 * @step5 { Array } menus ==> 根据menuIds 查询对应的菜单 memus 并取到menus
 * @step6 { Array } menus ==> 根据 perfMenuObj(item) 方法处理成前端要用的菜单
 * @step6 { Array } memuTree ==> 根据 tranListToTree(menus) 方法处理成树形数据
 */
router.get('/getRouters', (req, res) => {
  if (req.headers.authorization) {
    let token = req.headers.authorization.split(' ')[1];
    const loginInfo = verifyToken(token);
    if (loginInfo) {
      sqlUserIdByUserName = `select* from sys_user where user_name = '${loginInfo.username}'`;
      db.queryAsync(sqlUserIdByUserName).then(res_user => {
        const user = res_user.results[0];
        const sqlRoleId = `select* from sys_user_role where user_id = '${user.user_id}'`;
        db.queryAsync(sqlRoleId).then(role => {
          const roleId = role.results[0].role_id;
          if (!roleId) {
            res.send({
              code: 500,
              msg: '操作失败',
              data: []
            });
          } else if (roleId == 1) {
            // 如果是超级管理员
            const sql = `select* from sys_menu`;
            db.queryAsync(sql).then(res_menus => {
              const menus = res_menus.results.map(item => {
                return perfMenuObj(item);
              });
              let memuTree = tranListToTree(menus, 'menu_id', 'parent_id');
              const result = {
                code: 200,
                msg: '操作成功',
                data: memuTree
              };
              res.send(result);
            });
          } else {
            const sqlMenuIdsByRoleId = `select* from sys_role_menu where role_id = '${roleId}'`;
            db.queryAsync(sqlMenuIdsByRoleId).then(async res_menuIds => {
              // menu_id 与 role_id的对象
              const menuRole = res_menuIds.results;
              // 拿到全部的菜单id
              const menuIds = menuRole.map(item => item.menu_id);

              const menuIdsStr = menuIds.join(',');
              const sqlMenuByMenuId = `select* from sys_menu where menu_id in (${menuIdsStr})`;
              db.queryAsync(sqlMenuByMenuId).then(res_menus => {
                const menus = res_menus.results.map(item => {
                  return perfMenuObj(item);
                });
                let memuTree = tranListToTree(menus, 'menu_id', 'parent_id');
                const result = {
                  code: 200,
                  msg: '操作成功',
                  data: memuTree
                };
                res.send(result);
              });
            });
          }
        });
      });
    } else {
      res.status(401).send({ code: '401', msg: '登录失效，请重新登录' });
    }
  }
});

// 退出登录
router.post('/logout', (req, res) => {
  res.send('退出登录');
});

module.exports = {
  sysRouter: router
};
