/*
 * @Description: ------ 文件描述 ------
 * @Creater: snows_l snows_l@163.com
 * @Date: 2023-04-15 19:00:39
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2024-03-20 09:46:51
 * @FilePath: /ruoyi+node/ruoyiServer/src/index.js
 */
const express = require('express');
// 对post请求的请求体进行解析
const bodyParser = require('body-parser');
// 主要用来解决客户端请求与服务端的跨域问题
const cors = require('cors');
// 引入路由
const { sysRouter } = require('./router/sys');

// 获取.env中的环境变量
const dotenv = require('dotenv').config({ path: './.env' });
// console.log('-------- dotenv --------', dotenv);
const jwtKey = process.env.APP_JWTKEY;
// console.log('-------- jwtKey --------', jwtKey);

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use('/sys', sysRouter);

// const { generateToken, verifyToken } = require('../utils/handleToken');

// 服务启动在3333端口
app.listen(3333, () => {
  console.log('-------- 服务启动了，运行在http://localhost:3333 --------');
});
