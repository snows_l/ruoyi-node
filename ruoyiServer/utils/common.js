/*
 * @Description: ------ 文件描述 ------
 * @Creater: snows_l snows_l@163.com
 * @Date: 2023-04-17 09:36:26
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2024-03-20 10:22:01
 * @FilePath: /ruoyi+node/ruoyiServer/utils/common.js
 */

const db = require('../utils/connDB');
const { verifyToken } = require('../utils/handleToken');

/**
 * @description: 根据token获取当前用户的用户信息
 * @param { String } token
 * @return { Object } 当前登录用户的用户信息
 */
function getUserInfoByToken(token) {
  const user = verifyToken(token);
  if (user) {
    const sql = `select* from sys_user where user_name = '${user.username}'`;
    db.queryAsync(sql).then(res => {
      return res.results[0];
    });
  }
}

/**
 * @description 构造树型结构数据
 * @param { Array } data 数据源
 * @param { String } id id字段 默认 'id'
 * @param { String } parentId 父节点字段 默认 'parentId'
 * @param { String } children 孩子节点字段 默认 'children'
 */
function tranListToTree(data, id, parentId, children) {
  let config = {
    id: id || 'id',
    parentId: parentId || 'parentId',
    childrenList: children || 'children'
  };
  var childrenListMap = {};
  var nodeIds = {};
  var tree = [];
  for (let d of data) {
    let parentId = d[config.parentId];
    if (childrenListMap[parentId] == null) {
      childrenListMap[parentId] = [];
    }
    nodeIds[d[config.id]] = d;
    childrenListMap[parentId].push(d);
  }
  for (let d of data) {
    let parentId = d[config.parentId];
    if (nodeIds[parentId] == null) {
      tree.push(d);
    }
  }
  for (let t of tree) {
    adaptToChildrenList(t);
  }
  function adaptToChildrenList(o) {
    if (childrenListMap[o[config.id]] !== null) {
      o[config.childrenList] = childrenListMap[o[config.id]];
    }
    if (o[config.childrenList]) {
      for (let c of o[config.childrenList]) {
        adaptToChildrenList(c);
      }
    }
  }
  function setLevel(t, level) {
    t.__level__ = level;
    if (!t.children || !t.children.length) return;
    t.children.forEach(t => setLevel(t, level + 1));
  }
  tree.forEach(t => setLevel(t, 0));
  return tree;
}

/**
 * @description: 将数据库获取的菜单改为前端可用菜单
 * @param { Object } singleMenuObj
 * @return { Object } Object
 */
function perfMenuObj(singleMenuObj) {
  return {
    alwaysShow: singleMenuObj.visible == 1 ? true : singleMenuObj.visible == 0 ? false : '',
    component: singleMenuObj.component || 'Layout',
    // component: singleMenuObj.visible == 1 ? true : singleMenuObj.visible == 0 ? false : '',
    meta: {
      title: singleMenuObj.menu_name,
      icon: singleMenuObj.icon,
      noCache: singleMenuObj.id_cache,
      link: null
    },
    name: singleMenuObj.path,
    path: '/' + singleMenuObj.path,
    redirect: '',
    menu_id: singleMenuObj.menu_id,
    parent_id: singleMenuObj.parent_id
  };
}

module.exports = {
  getUserInfoByToken,
  tranListToTree,
  perfMenuObj
};
